/*Ova klasa sluzi za lakse upravljanje stanjima u kojima igra moze da se nadje...
 * U nasem sluacju postoje 2 stanja:
 * - Stanje menija (Menu state)
 * - Stanje igranja (Play state) */

package com.mygdx.managers;

import com.mygdx.gamestates.GameState;
import com.mygdx.gamestates.EndState;
import com.mygdx.gamestates.PlayState;

public class GameStateManager {
	
	private GameState gameState;
	
	public static final int END=0;
	public static final int PLAY=1;
	
	public GameStateManager() {
		setState(PLAY);
	}
	public void setState(int state) {
		if(gameState!=null)
			gameState.dispose();
		if(state==END) {
			gameState=new EndState(this);
		}
		if(state == PLAY) {
			gameState=new PlayState(this);
		}
	}
	
	public void update(float dt) {
		gameState.update(dt);
	}
	
	public void draw() {
		gameState.draw();
	}
	
	
}
