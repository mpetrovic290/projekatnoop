package com.mygdx.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.mygdx.managers.GameStateManager;
import com.mygdx.tastatura.Tasteri;

public class EndState extends GameState{
	
	private SpriteBatch sb;
	private BitmapFont font;
	
	public EndState(GameStateManager gsm) {
		super(gsm);
		init();
	}
	
	public void init() {
		
		sb=new SpriteBatch();
		
		FreeTypeFontGenerator gen=new FreeTypeFontGenerator(Gdx.files.internal("Hyperspace Bold.ttf"));
		FreeTypeFontParameter par=new FreeTypeFontParameter();
		par.size=30;
		font=gen.generateFont(par);
		gen.dispose();
	}
	
	public void update(float dt) {
		handleInput();
	}
	
	public void draw() {
		sb.setColor(1,1,1,1);
		sb.begin();
		font.draw(sb,"Osvojili ste " + skor + " poena!",125,270);
		sb.end();
	}
	
	public void handleInput() {
		if(Tasteri.isPressed(Tasteri.SPACE)) {
			skor=0;
			gsm.setState(GameStateManager.PLAY);
		}	
	}
	
	public void dispose() {
		
	}
}
