/* Klasa GameState je nadklasa za sve klase stanja u kojima igra moze da se nadje*/

package com.mygdx.gamestates;

import com.mygdx.managers.GameStateManager;

public abstract class GameState {
	
	protected static long skor=0;
	protected GameStateManager gsm;
	
	protected GameState(GameStateManager gsm) {
		this.gsm=gsm;
	}
	
	public abstract void init();
	public abstract void update(float dt);
	public abstract void draw();
	public abstract void handleInput();
	public abstract void dispose();
	
}
