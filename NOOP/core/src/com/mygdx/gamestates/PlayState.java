package com.mygdx.gamestates;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.entiteti.Asteroid;
import com.mygdx.entiteti.Igrac;
import com.mygdx.entiteti.Projektil;
import com.mygdx.game.AsteroidsGame;
import com.mygdx.managers.GameStateManager;
import com.mygdx.tastatura.Tasteri;

public class PlayState extends GameState {
	
	private static int timer=0;
	
	private ShapeRenderer sr; //Deklaracija shape renderera koji sluzi za rednerovanje prostih oblika.
	private SpriteBatch sb; //Deklaracija sprite batch-a koji sluzi za renderovanje slika i teksta
	
	private BitmapFont font;
	
	private Igrac igrac;
	private ArrayList<Projektil> projektili;
	private ArrayList<Asteroid> asteroidi;
	
	public PlayState(GameStateManager gsm) {
		super(gsm);
		init();
	}

	@Override
	public void init() {
		
		sr=new ShapeRenderer();
		sb=new SpriteBatch();
		
		FreeTypeFontGenerator gen=new FreeTypeFontGenerator(Gdx.files.internal("Hyperspace Bold.ttf"));
		FreeTypeFontParameter par=new FreeTypeFontParameter();
		par.size=20;
		font=gen.generateFont(par);
		gen.dispose();
		
		projektili=new ArrayList<Projektil>();		
		
		igrac = new Igrac(projektili);
		
		asteroidi=new ArrayList<Asteroid>();		
		for(int i=0;i<4;i++)
			spawnujAsteroid(MathUtils.random(0,AsteroidsGame.WIDTH),MathUtils.random(0,AsteroidsGame.HEIGHT));
	}

	@Override
	public void update(float dt) {
		handleInput();
		
		//update igraca
		igrac.update(dt);
		
		//update projektila 
		for(int i=0;i<projektili.size();i++) {
			projektili.get(i).update(dt);
			if(projektili.get(i).Ukloni()) {
				projektili.remove(i);
				i--;
			}
		}
		
		//update asteroida
		for(int i=0;i<asteroidi.size();i++) {
			asteroidi.get(i).update(dt);
			if(asteroidi.get(i).Ukloni()){
				asteroidi.remove(i);
				i--;
			}				
		}
		
		
		//timer za spawnovanje asteroida
		timer+=1;
		if(timer%1200==0) {
			spawnujAsteroid(MathUtils.random(0,AsteroidsGame.WIDTH),MathUtils.random(0,AsteroidsGame.HEIGHT));
			timer=0;
		}
		
		//provera kolizije
		kolizija();
		
	}
	
	private void kolizija() {
		//projektil i asteroid
		for(int i=0;i<projektili.size();i++) {
			Projektil p=projektili.get(i);
			for(int j=0;j<asteroidi.size();j++) {
				Asteroid a=asteroidi.get(j);
				if(a.sadrzi(p.getX(),p.getY())) {
					skor+=a.getSkor();
					projektili.remove(i);
					i--;
					asteroidi.remove(j);
					j--;
					podeliAsteroid(a);
					break;
				}
			}
		}
		//igrac i asteroid
		float oblikx[]=igrac.getOblikX();
		float obliky[]=igrac.getOblikY();
		for(int i=0;i<oblikx.length;i++) {
			for(int j=0;j<asteroidi.size();j++) {
				Asteroid a=asteroidi.get(j);
				if(a.sadrzi(oblikx[i],obliky[i])) {
					asteroidi.remove(j);
					j--;
					podeliAsteroid(a);
					gsm.setState(GameStateManager.END);
					break;
				}
			}
		}
	}
	
	private void podeliAsteroid(Asteroid a) {
		if(a.getTip()==Asteroid.VELIKI) {
			asteroidi.add(new Asteroid(a.getX(),a.getY(),Asteroid.SREDNJI));
			asteroidi.add(new Asteroid(a.getX(),a.getY(),Asteroid.SREDNJI));
		}
		if(a.getTip()==Asteroid.SREDNJI) {
			asteroidi.add(new Asteroid(a.getX(),a.getY(),Asteroid.MALI));
			asteroidi.add(new Asteroid(a.getX(),a.getY(),Asteroid.MALI));
		}
	}
	
	private void spawnujAsteroid(float x, float y) {		
		asteroidi.add(new Asteroid(x,y,Asteroid.VELIKI));
	}

	@Override
	public void draw() {
		
		igrac.draw(sr);
		
		for(int i=0;i<projektili.size();i++) {
			projektili.get(i).draw(sr);
		}
		
		for(int i=0;i<asteroidi.size();i++) {
			asteroidi.get(i).draw(sr);
		}
		
		sb.setColor(1,1,1,1);
		sb.begin();
		font.draw(sb,skor+"",40,450);
		sb.end();
	}

	@Override
	public void handleInput() {
		igrac.setLevo(Tasteri.isDown(Tasteri.LEVO));
		igrac.setDesno(Tasteri.isDown(Tasteri.DESNO));
		igrac.setGore(Tasteri.isDown(Tasteri.GORE));
		if(Tasteri.isPressed(Tasteri.SPACE))
			igrac.pucaj();
	}

	@Override
	public void dispose() {
		
	}
	
}
