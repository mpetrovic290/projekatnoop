/* Klasa tasteri sluzi za lakse proveravanje tastera...
 * U njoj su deklarisane konstane svih tastera koji se koriste u igri...
 * Uz pomoc metode isPressed mozemo dobiti samo jednu true vrednost da li je taster pritisnut
 * za razliku od metode isDown koja ce kontinualno vracati vrednost true u svakom frejmu u kom
 *  je taster pritisnut. */


package com.mygdx.tastatura;

public class Tasteri {
	
	private static final int BROJ_TASTERA=8;
	
	private static boolean[] tasteri;
	private static boolean[] ptasteri;
	
	static {
		tasteri=new boolean[BROJ_TASTERA]; 
		ptasteri=new boolean[BROJ_TASTERA];
	}
	
	public static final int GORE=0;
	public static final int LEVO=1;
	public static final int DOLE=2;
	public static final int DESNO=3;
	public static final int ENTER=4;
	public static final int ESC=5;
	public static final int SPACE=6;
	public static final int SHIFT=7;
	
	public static void update() {
		for(int i =0 ;i<BROJ_TASTERA;i++)
			ptasteri[i]=tasteri[i];
	}
	
	public static void setTaster(int k, boolean b) {
		tasteri[k]=b;
	}
	
	public static boolean isDown(int k) {
		return tasteri[k];
	}
	
	public static boolean isPressed(int k) {
		return tasteri[k] && !ptasteri[k];
	}
}
