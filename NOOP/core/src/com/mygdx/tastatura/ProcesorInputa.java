/* Procesor inputa uz pomoc klase Tasteri proverava proverava kada su tasteri pritisnuti (key down)
 * i kada su pusteni (key up)*/

package com.mygdx.tastatura;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.Input.Keys;

public class ProcesorInputa extends InputAdapter {
	public boolean keyDown(int k) {
		if(k==Keys.UP)
			Tasteri.setTaster(Tasteri.GORE,true);
		if(k==Keys.LEFT)
			Tasteri.setTaster(Tasteri.LEVO,true);
		if(k==Keys.DOWN)
			Tasteri.setTaster(Tasteri.DOLE,true);
		if(k==Keys.RIGHT)
			Tasteri.setTaster(Tasteri.DESNO,true);		
		if(k==Keys.ENTER)
			Tasteri.setTaster(Tasteri.ENTER,true);	
		if(k==Keys.ESCAPE)
			Tasteri.setTaster(Tasteri.ESC,true);	
		if(k==Keys.SPACE)
			Tasteri.setTaster(Tasteri.SPACE,true);
		if(k== Keys.SHIFT_RIGHT && k==Keys.SHIFT_LEFT);
			Tasteri.setTaster(Tasteri.SHIFT,true);	
		return true;
	}
	
	public boolean keyUp(int k) {
		if(k==Keys.UP)
			Tasteri.setTaster(Tasteri.GORE,false);
		if(k==Keys.LEFT)
			Tasteri.setTaster(Tasteri.LEVO,false);
		if(k==Keys.DOWN)
			Tasteri.setTaster(Tasteri.DOLE,false);
		if(k==Keys.RIGHT)
			Tasteri.setTaster(Tasteri.DESNO,false);		
		if(k==Keys.ENTER)
			Tasteri.setTaster(Tasteri.ENTER,false);	
		if(k==Keys.ESCAPE)
			Tasteri.setTaster(Tasteri.ESC,false);	
		if(k==Keys.SPACE)
			Tasteri.setTaster(Tasteri.SPACE,false);
		if(k== Keys.SHIFT_RIGHT && k==Keys.SHIFT_LEFT);
			Tasteri.setTaster(Tasteri.SHIFT,false);	
		return true;
	}
}