/* Klasa SpaceObjekat je nadklasa svih klasa koje se nalaze u PlayState-u i one su
 * igrac, asteroid, projektil...
 * Ona u sebi ima sve sto je svojstveno za njene podklase...
 * Igrac, asteroid, projektil imaju svoju xy lokaciju, brzinu, sirinu, visinu,... */

package com.mygdx.entiteti;

import com.mygdx.game.AsteroidsGame;

public class SpaceObjekat {
	
	protected float x;
	protected float y;
	
	protected float dx;
	protected float dy;
	
	protected float radian;
	protected float brzina;
	protected float rotacionaBrzina;
	
	protected int sirina;
	protected int visina;
	
	protected float[] oblikx;
	protected float[] obliky;
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float[] getOblikX(){
		return oblikx;
	}
	
	public float[] getOblikY() {
		return obliky;
	}
	
	//metoda koja proverava da li odredjeni poligonalni oblik sadrzi zadate tacke
	public boolean sadrzi(float x, float y) {
		boolean b=false;
		for(int i=0,j=oblikx.length-1;i<oblikx.length;j=i++) {
			if((obliky[i]>y)!=(obliky[j]>y) &&
				(x<(oblikx[j]-oblikx[i])*
				(y-obliky[i])/(obliky[j]-obliky[i])+oblikx[i])){
					b=!b;
				}
		}
		return b;
	}
	
	protected void wrap() {
		if(x<0)x=AsteroidsGame.WIDTH;
		if(y<0)y=AsteroidsGame.HEIGHT;
		if(x>AsteroidsGame.WIDTH)x=0;
		if(y>AsteroidsGame.HEIGHT)y=0;
	}
	
}