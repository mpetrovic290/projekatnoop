package com.mygdx.entiteti;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Asteroid extends SpaceObjekat{
	
	private int skor;
	private int tip;
	public static final int MALI=0;
	public static final int SREDNJI=1;
	public static final int VELIKI=2;
	
	private int brojTacaka;
	private float distance[];
	
	private boolean ukloni;
	
	public Asteroid(float x,float y,int tip) {
		
		this.x=x;
		this.y=y;
		this.tip=tip;
		
		brojTacaka=MathUtils.random(8, 12);

		if(tip==MALI) {
			visina=sirina=15;
			brzina=MathUtils.random(70,100);
			skor=50;
		}		
		else if(tip==SREDNJI) {
			visina=sirina=25;
			brzina=MathUtils.random(50,60);
			skor=40;
		}		
		else if(tip==VELIKI) {
			visina=sirina=40;
			brzina=MathUtils.random(30,40);
			skor=30;
		}
		rotacionaBrzina=MathUtils.random(-1,1);
		
		radian=MathUtils.random(2*3.1415f);
		dx=MathUtils.cos(radian)*brzina;
		dy=MathUtils.sin(radian)*brzina;
		
		oblikx=new float[brojTacaka];
		obliky=new float[brojTacaka];
		distance=new float[brojTacaka];
		
		int poluprecnik=sirina/2;
		for(int i=0;i<distance.length;i++) 
			distance[i]=MathUtils.random(poluprecnik/2,poluprecnik);
		
		setShape();
	}
	
	private void setShape() {
		float ugao=0;
		for(int i=0;i<brojTacaka;i++) {
			oblikx[i]=x+MathUtils.cos(ugao+radian)*distance[i];
			obliky[i]=y+MathUtils.sin(ugao+radian)*distance[i];
			ugao+=2*3.1415f/brojTacaka;
		}		
	}
	
	public int getSkor() {
		return skor;
	}
	
	public int getTip() {
		return tip;
	}
	
	public boolean Ukloni() {
		return ukloni;
	}
	
	public void update(float dt) {		
		x+=dx*dt;
		y+=dy*dt;
		
		radian+=rotacionaBrzina*dt;
		setShape();
		
		wrap();
	}
	
	public void draw(ShapeRenderer sr) {
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Line);
		
		for(int i=0,j=oblikx.length-1; i<oblikx.length;j=i++) {
			sr.line(oblikx[i],obliky[i],oblikx[j],obliky[j]);
		}
		
		sr.end();
	}
}
