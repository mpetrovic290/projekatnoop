package com.mygdx.entiteti;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;

public class Projektil extends SpaceObjekat{
	
	private float trajanje;
	private float vremeTrajanja;
	
	private boolean ukloni;
	
	public Projektil(float x, float y, float radian) {
		this.x=x;
		this.y=y;
		this.radian=radian;
		
		brzina=350;
		dx=MathUtils.cos(radian)*brzina;
		dy=MathUtils.sin(radian)*brzina;
		
		sirina=visina=2;
		
		trajanje=0;
		vremeTrajanja=1;
	}
	
	public boolean Ukloni() {
		return ukloni;
	}
	
	public void update(float dt) {
		x+=dx*dt;
		y+=dy*dt;
		
		wrap();
		trajanje+=dt;
		if(trajanje>vremeTrajanja)
			ukloni=true;		
	}
	public void draw(ShapeRenderer sr) {
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Line);
		sr.line(x, y, x+2, y+2);
		sr.end();
	}
	
}
