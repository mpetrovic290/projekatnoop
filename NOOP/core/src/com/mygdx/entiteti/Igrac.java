package com.mygdx.entiteti;

import java.util.ArrayList;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.mygdx.game.AsteroidsGame;

public class Igrac extends SpaceObjekat {
	//Promenljive pomocu kojih proveravamo da li su tasteri pritisnuti
	private boolean levo;
	private boolean desno;
	private boolean gore;
	
	//Dodatne promenljive koje uticu na kretanje igraca
	private float maxBrzina;
	private float ubrzanje;
	private float frikcija;
	
	private final int MAX_PROJEKTILA = 4;
	private ArrayList<Projektil> projektili;
	
	//Konstruktor igraca
	public Igrac(ArrayList<Projektil> lista) {
		x=AsteroidsGame.WIDTH/2;
		y=AsteroidsGame.HEIGHT/2;
		
		brzina=3;
		maxBrzina=300;
		ubrzanje=200;
		frikcija=10;
		
		oblikx=new float[4];
		obliky=new float[4];
		
		radian = 3.1415f/2.5f;			
		rotacionaBrzina=3;
		
		this.projektili=lista;
	}
	
	//Postavljanje oblika sa trigonometrijskim metodama
	private void setOblik() {
		oblikx[0]=x+MathUtils.cos(radian)*8;
		obliky[0]=y+MathUtils.sin(radian)*8;		
		
		oblikx[1]=x+MathUtils.cos(radian-4*3.1415f/5)*8;	
		obliky[1]=y+MathUtils.sin(radian-4*3.1415f/5)*8;
		
		oblikx[2]=x+MathUtils.cos(radian+3.1415f)*5;	
		obliky[2]=y+MathUtils.sin(radian+3.1415f)*5;
		
		oblikx[3]=x+MathUtils.cos(radian+4*3.1415f/5)*8;
		obliky[3]=y+MathUtils.sin(radian+4*3.1415f/5)*8;
	}
	
	//Metode koje proveravaju da li su tasteri pritisnuti
	public void setLevo(boolean b) {
		levo=b;
	}
	
	public void setDesno(boolean b) {
		desno=b;
	}
	
	public void setGore(boolean b) {
		gore=b;
	}
	
	public void pucaj() {
		if(projektili.size()==MAX_PROJEKTILA)
			return;
		projektili.add(new Projektil(x,y,radian));
	}
		
	//Metoda koja u svakom frejmu azurira lokaciju i rotaciju igraca
	public void update(float dt) {
		//Okretanje letelice
		if(levo) {
			radian+=rotacionaBrzina*dt;			
		}
		else if(desno) {
			radian-=rotacionaBrzina*dt;
		}
		
		//Ubrzavanje letelice
		if(gore) {
			dx+=MathUtils.cos(radian)*ubrzanje*dt;
			dy+=MathUtils.sin(radian)*ubrzanje*dt;
		}
		
		float vec=(float)Math.sqrt(dx*dy);
		
		if(vec>0) {
			dx-=(dx/vec)*frikcija*dt;
			dy-=(dy/vec)*frikcija*dt;
		}
		
		if(vec>maxBrzina) {
			dx=(dx/vec)*maxBrzina;
			dy=(dy/vec)*maxBrzina;
		}
		
		x+=dx*dt;
		y+=dy*dt;
		
		setOblik();
		
		wrap();
	}
	
	public void draw(ShapeRenderer sr) {
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Line);
		
		for(int i=0,j=oblikx.length-1; i<oblikx.length;j=i++) {
			sr.line(oblikx[i],obliky[i],oblikx[j],obliky[j]);
		}
		sr.setColor(1,0,0,1);
		if(gore) {
			sr.line(x+MathUtils.cos(radian+5*3.1415f/6)*6,
					y+MathUtils.sin(radian+5*3.1415f/6)*6,
					x+MathUtils.cos(radian+3.1415f)*14,
					y+MathUtils.sin(radian+3.1415f)*14);
		
			sr.line(x+MathUtils.cos(radian-5*3.1415f/6)*6,
					y+MathUtils.sin(radian-5*3.1415f/6)*6,
					x+MathUtils.cos(radian+3.1415f)*14,
					y+MathUtils.sin(radian+3.1415f)*14);
		}
		sr.end();
	}
}
