/* Klasa AsteroidsGame je glavna klasa ovog programa...*/


package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.managers.GameStateManager;
import com.mygdx.tastatura.ProcesorInputa;
import com.mygdx.tastatura.Tasteri;

public class AsteroidsGame extends ApplicationAdapter {
	
	//Visina i sirina prozora
	public static int WIDTH;
	public static int HEIGHT;
	
	//Ortografska kamera koja je uperena u ono sto se prikazuje
	public static OrthographicCamera cam;
	
	//Menadzer stanja igre
	private GameStateManager gsm;
	
	@Override
	public void create () {
		WIDTH=Gdx.graphics.getWidth();
		HEIGHT=Gdx.graphics.getHeight();
		
		cam= new OrthographicCamera(WIDTH,HEIGHT); //Pozivanje konstruktora za klasu kamera
		cam.translate(WIDTH/2,HEIGHT/2); //Translacija kamere
		cam.update();//Azuriranje kamere nakon translacije
		
		Gdx.input.setInputProcessor(new ProcesorInputa());//Postavljanje default procesora inputa za nasu klasu
		
		gsm=new GameStateManager();
	}
	
	@Override
	public void render () {
		
		Gdx.gl.glClearColor(0, 0, 0, 1); //Metoda koja postavlja boju kojom cemo obojiti pozadinu
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);//Metoda koja boji pozadinu izabranom bojom
		
		gsm.update(Gdx.graphics.getDeltaTime());
		gsm.draw();
		
		Tasteri.update();
	}
}
